\documentclass{patmorin}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{pat}
\usepackage{hyperref}
\usepackage{tikz}
\usepackage{fancyvrb}
\usepackage{cite}


\usepackage{lipsum}


\newtheorem{theorem}{Theorem}
\hypersetup{colorlinks=true, linkcolor=linkblue,  anchorcolor=linkblue,
citecolor=linkblue, filecolor=linkblue, menucolor=linkblue,
urlcolor=linkblue, pdfcreator=Me, pdfproducer=Me} 
\setlength{\parskip}{1ex}

\newcommand{\eps}{\varepsilon}
\newcommand*{\QEDA}{\hfill\ensuremath{\blacksquare}}%



\title{\MakeUppercase{Searching On An Infinite Line : A Deterministic Asynchronous Rendezvous Of Two Agents}}
\author{
Christopher Blackman\thanks{Carleton University}}



\begin{document}
\maketitle

\begin{abstract}
Two mobile agents which have distinct labels are located on an infinite line. Both agents are asynchronous, and each one is controlled by an algorithm dependent on its finite label. Agents do not know orientation, or topology, nor can they communicate with other agents. The \textit{competitive ratio} is defined by the total worst case distance travelled by the agents divided by the initial distance between the agents; this initial distance is denoted by $D$. We show that for an infinite line with unknown distance $D$, the competitive ratio is $O(b^{O({|L_{min}|})})$, such that $b>1$, and we obtain a constant ratio. Where $L_{min}$ is the minimum label size.
\end{abstract}

\section{Introduction}
\subsection{Acknowledgements}
I would like to thank my advisers Prosenjit Bose, and Jean-Lou De Carufel for their guidance, and support for this research. I would also like to thank Pat Morin for this latex template, and fellow colleagues for their support.
\subsection{Problem}
\paragraph{}
Two mobile agents are on an infinite line. 
They know neither orientation, nor distance from each other.
In order to create the worst case in an asynchronous rendezvous, an adversary controls whether an agent moves, or not at any point in time.
If an agent has traversed a section of the line already, it has control over it's orientation, but not its speed.
However, it is assumed that velocity is always non-zero.
These agents follow an algorithm dependent on it's assigned label.
We define these labels as $L_{max}$, and $L_{min}$, such that $L_{max}$ is the larger label of the two, and $L_{min}$ is the smaller label.

\paragraph{}
Many initial works involve a synchronous setting for rendezvous where both agents execute at the same time. Although the synchronous setting will not be studied in this paper, the books made by Steve Alpern et al. \cite{test5,test6} are a good introduction to search games, geometric games, and rendezvous problems. An example of a search game could be the optimal solution to finding an agent in a bounded rectangle where the agent is attempting to escape the searcher, and neither knows the others position. However, we will be focusing on rendezvous instead, such that both agents are attempting to meet.

\paragraph{}
The first worked introduced for asynchronous rendezvous was seen in the paper by De Marco et al. \cite{test2} where they proved that is possible on lines, on rings, and arbitrary graphs. The topic we will be focussing are lines where distance between both agents are unknown to each other. De Marco et al proved that for known initial distances on lines that an upper bound on asynchronous rendezvous is $O(D|L_{min}|^2)$, and for unknown initial distance there is an upper bound of $O((D+L_{max})^3)$.

\paragraph*{}
The work by Grzegorz Stachowiak \cite{test3} proved a lower constant for known initial distance on lines, as well proved an asymptotic $O(D\log^2D + D\log D|L_{max}| + D|L_{min}|^2 + |L_{max}|\log |L_{min}|)$ upper-bound for lines of unknown initial distance.


\paragraph*{}
In this paper we focus on applying the same technique for searching on an infinite line seen in \cite{test4} , or searching on m-rays (where $m=2$) seen in \cite{test7}. Both of these papers employ an exponential techniques on their search function, and prove optimality for a competitive ratio of O(9). We hope that searching for a point on a line has similar implications for rendezvous on a line.

\subsection*{Contributions}
\paragraph*{}
Our contributions for asynchronous rendezvous proves that $\frac{3b^4 D}{(b-1)^2(b+1)} \cdot b^{2\cdot O(|L_{min}|)}$ is also an upper-bound to the problem; such that $b>1$. This has the advantage compared to other works in which it is constantly competitive in respect to $D$, however, it is exponential in respect to label size. This means that in some cases depending on initial distance the previously proved works \cite{test2,test3} preform better, but as $D$ tends towards infinity and our label size is fixed, then our algorithm outperforms theirs. 

\paragraph*{}
We have two cases, one where the minimum label size is known, and another where the minimum label size is unknown. We show that if minimum label size is known, then it is possible to calculate a constant competitive ratio. In addition, if no knowledge of label size is known, then one can choose a $b=2.563$ as constant that minimizes the constant of the equation such that the competitive ratio is the following : $29.8 \cdot (2.563)^{4\cdot |L_{min}| + 15}$.

\paragraph*{}
Subsequently, in Theorem \ref{lemma-convergent}, we prove that there exists a convergent bit every $O(|L_{min}|)$ bit positions. This improves the bound in the paper by De Marco et al. \cite{test2} from $O((D+L_{max})^3)$ to $O((D+L_{min})^3)$ for rendezvous on a line of unknown initial distance. This is true since we use a very similar proof technique used in their paper.

\subsection{Model}
\paragraph*{}
\textit{The network} : is defined as an infinite line, where agents are points moving on the line. This problem can be translated to general graphs as seen in \cite{test2}.

\paragraph*{}
\textit{Adversarial decisions, definition of rendezvous and its cost } : An agent located on the fringe of an untravelled portion of the line has no knowledge of what lies on that line.
If an agent decides to travel the untravelled portion, then the Adversarial decides which initial orientation, and subsequent movement speeds which leads to the worst case.
The decision aims for the worst case, such that topology and orientation are unknown to the agents, but known to the Adversarial.
An agent may decide to travel on an already travelled line segment, then this decision depends on an algorithm that is reliant on label $L$.


\paragraph*{}
Movement in the network can be any arbitrary speed as long as the movement is continuous. 
Moreover, the Adversarial may choose the movement speed of any agent at any time.

\paragraph*{}
For a given algorithm, given starting points, and a sequence of Adversarial decisions in an infinite line, a rendezvous occurs if both agents are in the same point at the same time. We say that a rendezvous is \textit{feasible} on an infinite line, if there exists an algorithm such that for any starting points, and any sequence of Adversarial decisions, rendezvous occurs. The cost, or upper bound is the worst-case distance travelled in order to rendezvous by both agents. While, the lower bound is the initial distance apart $D$.

\paragraph*{}
\textit{Labels and local knowledge:}
If agents are identical \textit{(ie. they do not have distinct labels)}, then the Adversarial can always make the agents never meet, therefore distinct labels are assumed.
Labels are defined as binary bit strings, such that $L_{max}$ denotes the larger label, and $L_{min}$ denotes the smaller label (it can also be the case that $|L_{max}| = |L_{min}|$).
If both agents know each other's labels, then upon some choice (ie. smaller, or larger) one will wait and another will search.
This reduces down to searching on a line, and thus will cost no more than $9D$ \cite{test4}, such that $D$ is the initial distance between both agents.

\section{Deterministic Algorithm for Unknown  Distance}
\subsection{Proposed Algorithm}
\noindent
\textbf{Label Transformation : } The label transformation takes label $L$ and transforms it into $L^*$ which will be used by the algorithm, where $L$ is a distinct bit-string.
\newline
\noindent\textit{Step 1 :} Let $L'$ be obtained by inserting a new bit before every other bit in $L$ alternating between $'0'$, and $'1'$. For example if we had label $L='XXX'$, then $L'='0X1X0X'$.
\newline
\noindent\textit{Step 2 :} Let $L''$ be obtained by appending the string $'11110'$ at the end of $L'$, such that if we have $L'='0X1X0X'$, then $L''='0X1X0X11110'$.
\newline
\noindent\textit{Step 3 :} Let $L*$ be an infinite concatenation of copies of $L''$.
\newline
\noindent
\textbf{Label Execution : } For the execution of the $i$th bit which $L^*(i)=0$, our agent performs $b^{2i}$ steps to the left, $b^{2i} + b^{2i + 1}$ steps to the right, and returns left $b^{2i + 1}$ steps to its starting position. If $L^*(i)=1$, then our agent starts to the right instead. It is noted that in order for this to work, $b$ must be greater than $1$; we will prove this in Theorems \ref{lemma-threshold-bit}, and Section \ref{lemma-sum}.

\newpage

\subsubsection{Fact}
\begin{enumerate}
\item \textit{L* does not contain the the string 0000.}
\item \textit{The only place where the substring 11110 exists is at the end of a copy of $L'$.} \label{fact-2}
\item \textit{The strings $\{1111,0000\} \notin$ any substrings of $L'$}\label{fact-3}
\item \textit{For any labels $L_1,L_2$, if $L_1 \neq L_2$, then $L_1' \neq L_2'$.}\label{fact-4}
\item \textit{The cost for executing one byte in a label is $3b^{2i+1}$, where $i$ is the executed byte position}
\item \textit{For any bit position $n$ in $L^*$, the execution from 1 to $n$ bits requires $\sum_{i=1}^{n}3b^{2i+1}$} \label{fact-6}
\end{enumerate}

\begin{definition}
\noindent
\textit{Definitions : }A \textit{`convergent bit'} is defined when both agents start facing each other.
A \textit{`divergent bit'} is defined when both agents start and are not facing each other.
A \textit{`same bit'} is defined when both agents start in the same direction.
\end{definition}

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{figs/configurations}
\caption{Showing a) convergent bit position, b) divergent bit position, c) same bit position}
\label{figure-iqndex}
\end{figure}

\subsection{Existence of a Convergent/Divergent Bit}
\begin{theorem} \label{lemma-convergent}
\textit{Let $X$ and $Y$ be the two agents. For any $p > 1$, there exists a $q > p$ such that $q-p \le O(|L_{min}|)$ and the direction of X on $L_X^*(q)$ is different from that of $Y$ on $L_Y^*(q)$.}  
\end{theorem}
\\
\\
\noindent In order to prove Theorem \ref{lemma-convergent} we must show that the case of when both $L_{max}$, and $L_{min}$ equal in size; and the case when both $L_{max}$ and $L_{min}$ differ in size. We use two facts to prove this 1) that both labels are distinct, 2) that there is an upper bound on the largest common sub-string. This will be seen in the following cases:
\\
\\
\noindent\textbf{Case 1)} \textit{Let $|L_{min}|=|L_{max}|$ ie : X and Y are the same size.}\\
Let $r$ be the bit position before the first occurrence of the string \textit{11110} after bit $p$. Let  $c$ be the bit position before the second occurrence of the string \textit{11110}. This string is only seen at the end of each label transformation from \textit{fact} \ref{fact-2}, and both labels of X and Y are aligned \textit{(11110 appears in same bit positions of X, and Y)} since they are of equal size. Therefore we can say that $L_X^*(r+5,c)=L_X'$, and $L_Y^*(r+5,c)=L_Y'$. We know that labels $L_X$ and $L_Y$ are distinct, and that the transformation of these labels also creates distinct labels $L_X'$ and $L_Y'$ from \textit{fact} \ref{fact-4}. Therefore there is a convergent bit $q$ that lives between bits $r+5\le q\le c$, such that $q-p \le c-p \le 2|L_{min}''|= 4|L_{min}| + 10 = O(|L_{min}|)$. This is true since the distance from $p$ to $c$ is at most $2|L''_{min}|$ since we travel at most one $|L''_{min}|$ to get to $r$, and another $L''$ to get to $c$.
\\
\\
\noindent\textbf{Case 2)} \textit{Let $|L_{min}|<|L_{max}|$ ie : X and Y are of different size.}\\
If we go from left to right reading the labels marking all common sub-strings between labels X and Y, then at the end of each sub-string, there exists a convergent bit.
Therefore we will prove that there exists a convergent bit in at most $s + 1= (2|L'_{min}| + 15) + 1 = O(|L_{min}|)$, where $s$ is the largest common sub-string (LCSS) between labels X, and Y.
By showing that the maximum size of the LCSS, we know that exists a convergent bit $q$ such that $p-q \le O(|L_{min}|)$.
\\\noindent\textbf{Case 2.1)}When \textit{$11110 \notin LCCS$}\\
Since the string $11110$ always appears at the end of $L''_{min}$, then the upper bound is \\$s=4 + |L'_{min}| + 4 = |L'_{min}| + 8$.
\\\noindent\textbf{Case 2.2)}When \textit{$11110 \in LCCS$}\\
Our LCSS starts off with $11110$ aligned, where the common sub-string spans left/right in $|L_{min}|$, and then starts/ends in $11110$.
In the case where $|L_{max}|-|L_{min}| >= 5$, then $11110$ lands inside $L'_{max}$. Since $1111 \notin L'$ by fact \ref{fact-3}, then there is a convergent bit in $s=2|L'_{min}|+15$.
In the case where $ 1 \le |L_{max}|-|L_{min}| < 5$, then there is always a convergent bit in $11110$ when it is off by one index as seen in figure \ref{figure-index}.
This is also bounded by $s=2|L'_{min}|+15$.

\QEDA

\begin{figure}[h]
\centering
\begin{BVerbatim}
11110X | 11110XX | 11110XXX | 11110XXXX
X11110 | XX11110 | XXX11110 | XXXX11110
\end{BVerbatim}
\caption{Convergent bit of string $11110$ when off by at-most 4 bits}
\label{figure-index}
\end{figure}

% CASE 1 ---------------


\subsection{Worst Case Bit Required to meet}

\begin{theorem}\label{lemma-threshold-bit}
There exists a threshold bit $p=\frac{\log_b(\frac{D}{b-1})+1}{2}$, such that after reaching this threshold both agents will rendezvous in at most $\frac{log_b(\frac{D}{b-1})+1}{2} + O(|L_{min}|)$.
\end{theorem}
\\
\\
\noindent In order to prove Theorem \ref{lemma-threshold-bit}, we must calculate all possible thresholds bit values for meeting in convergent positions, divergent positions, and same bit positions. By doing so we cover all possible states in which agents may be in dependent on their label execution. The general arrangement for our proofs find the worst case minimum bit-position $p$ needed for rendezvous. We use this fact to prove that any bit above $p$ will also result in rendezvous. This will be a general concept in proving cases one through five.
\\
\\
\noindent\textbf{Case 1) } Both agents are convergent and executing bit $p$, and both are executing the first segment. Then the most that one needs to travel is at most distance $D$.
\begin{align*}
  b^{2p} = D\\
  p = \frac{\log_bD}{2}
\end{align*}

Let $m>0$ be an integer. Let $p' = p + m$. We prove that any bit above $p$ will also converge.

\begin{align*}
  b^{2p'} \ge D \\
  b^{\log_bD + 2m} \ge D\\
  b^{2m}\cdot D \ge D\\
\end{align*}
Therefore if we choose a $b>1$ for this particular threshold $p$, our algorithm will converge for this particular case.\\


% CASE 2 ---------------




\noindent\textbf{Case 2) } Both agents are convergent. Agent one is executing bit $p$, and the other agent is still executing bit $p-1$. Agent one is in the first segment. Agent two is assumed to be in segment two and can be at most $b^{2(i-1)+1}=b^{2i-1}$ in any direction. Therefore, in the worst case Agent one must travel $b^{2i-1}+D$ in order to meet with agent two.
\begin{align*}
  b^{2p} =& b^{2p-1} + D\\
  p =& \frac{\log_b(\frac{D}{b-1})+1}{2}
\end{align*}

Let $m>0$ be an integer. Let $p' = p + m$. We prove that any bit above $p$ will also converge.

\begin{align*}
  b^{2p'}-b^{2p'-1} \ge D \\
  b^{\log_b(\frac{D}{b-1})+1+2m}-b^{\log_b(\frac{D}{b-1})+2m} \ge D \\
  b^{2m}\cdot b \cdot \frac{D}{b-1} - b^{2m} \cdot \frac{D}{b-1} \ge D\\
  b^{2m} \cdot \frac{D}{b-1}\cdot (b-1) \ge D\\
  b^{2m}\cdot D \ge D\\
\end{align*}
Therefore if we choose a $b>1$ for this particular threshold $p$, our algorithm will converge for this particular case.\\


% CASE 3 ---------------

\noindent\textbf{Case 3) }Both agents are divergent, and both are executing bit $p$. Let us say that agent one is trying to escape agent two. Then the most that agent one can be is $D + b^{2p}$ away from agent two. The only way for agent two to reach would be when agent two starts executing its second segment, therefore it must travel $b^{2p+1}$ from its origin towards agent one. Therefore the minimum bit needed for this convergence is the following:
\begin{align*}
b^{2p + 1} = D + b^{2p}\\
p = \frac{\log_b (\frac{D}{b-1})}{2}
\end{align*}

Let $m>0$ be an integer. Let $p' = p + m$. We prove that any bit above $p$ will also converge.

\begin{align*}
  b^{2p'+1}-b^{2p'} \ge D \\
  b^{\log_b(\frac{D}{b-1})+1+2m}-b^{\log_b(\frac{D}{b-1})+2m} \ge D \\
  b^{2m}\cdot b \cdot \frac{D}{b-1} - b^{2m} \cdot \frac{D}{b-1} \ge D\\
  b^{2m} \cdot \frac{D}{b-1}\cdot (b-1) \ge D\\
  b^{2m}\cdot D \ge D\\
\end{align*}

Therefore if we choose a $b>1$ for this particular threshold $p$, our algorithm will converge for this particular case.\\


% CASE 4 ---------------

\noindent\textbf{Case 4) }Both agents are divergent. Agent one is executing bit $p$, and agent two is still executing bit $p-1$. Agent one is in the second segment and heading in the direction of agent two. Agent two is assumed to be in segment two and can be at most $b^{2(i-1)+1}=b^{2i-1}$ in any direction. Therefore, in the worst case agent one must travel $b^{2i-1}+D$ in order to rendezvous with agent two.
\begin{align*}
b^{2p+1} =& b^{2p-1} + D\\
p =& \frac{\log_b(\frac{D}{b^2-1})+1}{2}
\end{align*}

Let $m>0$ be an integer. Let $p' = p + m$. We prove that any bit above $p$ will also converge.

\begin{align*}
  b^{2p'+1}-b^{2p'-1} \ge D \\
  b^{\log_b(\frac{D}{b^2-1})+2+2m}-b^{\log_b(\frac{D}{b^2-1})+2m} \ge D \\
  b^{2m}\cdot b^2 \cdot \frac{D}{b^2-1} - b^{2m} \cdot \frac{D}{b^2-1} \ge D\\
  b^{2m} \cdot \frac{D}{b^2-1}\cdot (b^2-1) \ge D\\
  b^{2m}\cdot D \ge D\\
\end{align*}

Therefore if we choose a $b>1$ for this particular threshold $p$, our algorithm will converge for this particular case.\\



% CASE 5 ---------------


\noindent\textbf{Case 5)}Both agents have the same direction, and at least one agent has passed the threshold bit $p$.
The threshold bit is defined as $p$ = $max(\frac{\log_bD}{2},\frac{log_b(\frac{D}{b-1})+1}{2},\frac{log_b(\frac{D}{b^2-1})+1}{2})=\frac{log_b(\frac{D}{b-1})+1}{2}$.
You can see this clearly when you represent them in the following way for $b>1$:
\begin{align*}
  {\log_bD} :& {log_b(\frac{D}{b-1})+1} : {log_b(\frac{D}{b^2-1})+1}\\
  {\log_bD} :& {\log_b{D}-\log_b{(b-1)}+1} : {\log_b{D}-\log_b{(b^2-1)}+1}
\end{align*}
\noindent\textbf{Case 5.1)} Agent one is facing agent two, and agent one is on some bit $p$, while agent two is on some bit less than $p$.
There are two possible outcomes to avoid convergence : first agent two completes before agent one completes; second agent two tries go as far as possible from agent one.
We will show that no-matter how far agent two goes, agent two will get caught unless agent two completes before agent one.
\\
\\
Let $p'=p + m$ such that $m\ge 0$. Let $p'' = p+ m -n$ such that $n > 0$. Both $p',p''$ correspond respectively with agent one, and two. 
Since agent two wants to get as far as possible from agent one, then it is seen that $b^{2(p+m-n)}\le b^{2(p+m-1)}=b^{2(p'-1)}$.
Therefore the following is true:
\begin{align*}
  b^{2p'} - b^{2(p'-1)+1} \ge D\\
  b^{2(\frac{log_b(\frac{D}{b-1})+1}{2} + m)} - b^{2((\frac{log_b(\frac{D}{b-1})+1}{2})-1+m)+1} \ge D\\
  \frac{D}{b-1}b^{2m}b-\frac{D}{b-1}b^{2m} \ge D\\
  Db^{2m} \ge D
\end{align*}
Therefore the only way for agent two to escape agent one would be to finish before agent one.

\noindent\textbf{Case 5.2)} Agent one faces agent two, and agent one is on some bit less than $p$, while agent two is on some bit $p$.
There are two possible outcomes to avoid convergence : first agent two completes before agent one completes; second agent two tries go as far as possible from agent one.
We will show that no-matter how far agent one goes, agent one will get caught unless agent one completes before agent two.
\\
\\
Let $p'=p + m$ such that $m\ge 0$. Let $p'' = p+ m -n$ such that $n > 0$. Both $p',p''$ correspond respectively with agent one, and two. 
Since agent two wants to get as far as possible from agent one, then it is seen that $b^{2(p+m-n)+1}\le b^{2(p+m-1)+1}=b^{2(p'-1)+1}$.
Therefore the following is true:
\begin{align*}
  b^{2p'+1} - b^{2(p'-1)+1} \ge D\\
  b^{2(\frac{log_b(\frac{D}{b-1})+1}{2} + m)+1} - b^{2((\frac{log_b(\frac{D}{b-1})+1}{2})-1+m)+1} \ge D\\
  \frac{D}{b-1}b^{2m}b^2-\frac{D}{b-1}b^{2m} \ge D\\
  Db^{2m}(b+1) \ge D
\end{align*}
Therefore the only way for agent one to escape agent two would be to finish before agent two.

\noindent\textbf{Case 5.3)} Both agents are on bit $p$.
We have seen in cases 5.1, and 5.2 that once past the threshold bit, the lower bit must escape to a higher bit, else both agents will rendezvous.
This means once an agent is on the threshold bit, then the other agent must also get on the same bit otherwise rendezvous.
However, we know from 2.3 that there exists a convergent bit in $O(|L_{min}|)$, therefore they will rendezvous in at most $\frac{log_b(\frac{D}{b-1})+1}{2} + O(|L_{min}|)$.

\QEDA

\section{Correctness and Analysis}\label{lemma-sum}
In order for the agents to intersect they must execute at least $n$ many bits:
$$n = \frac{\log_b(\frac{D}{b-1})+1}{2} + \Delta \le \frac{\log_b(\frac{D}{b-1})+1}{2} + O(|L_{min}|)$$
Such that $\Delta$ is the number of bits it takes for both agents to have a convergent bit after they have exceeded the threshold for the worst case minimum bit needed for convergence. It is noted that if a agent falls behind by a bit after this threshold bit, that both agents will converge. Therefore this is an upper bound. We can bound this delta by $|L_{min}|$ using the existence of a convergent bit in theorem \ref{lemma-convergent}, and \ref{lemma-threshold-bit}. We remind the fact that $D$ is unknown, and that $b$ is a value that we choose to optimize our upper bound. Since we have a worst case bit $n$, we use fact \ref{fact-6} in-order to find the upper-bound of distance travelled. 

\begin{align*}
\sum_{i=1}^{n}3b^{2i+1}
&=3b\sum_{i=1}^{n}b^{2i}\\
&=(3b)\Big(\frac{b^2(b^{2n}-1)}{(b^2-1)}\Big)\\
&=\frac{3b^3}{b^2-1}(b^{2n}-1)\\
&\le \frac{3b^3}{b^2-1}\cdot(b^{2n})\\
&= \frac{3b^3}{b^2-1}\cdot\bigg(b^{2\big(\frac{\log_b(\frac{D}{b-1})+1}{2} + O(|L_{min}|)\big)}\bigg)\\
&= \frac{3b^3}{b^2-1}\cdot\frac{D}{b-1} \cdot b \cdot b^{2\cdot O(|L_{min}|)}\\
&= \frac{3b^4 D}{(b-1)^2(b+1)} \cdot b^{2\cdot O(|L_{min}|)}\\
&= \frac{3b^4 D}{(b-1)^2(b+1)} \cdot b^{4\cdot |L_{min}| + 15}
\end{align*}
\paragraph*{}
If we take our lower bound $D$, then we get a competitive ratio : $\frac{3b^4}{(b-1)^2(b+1)} \cdot b^{4\cdot |L_{min}| + 15}$ dependent on $b$ and the label size. If $|L_{min}|=1$, then the best value for $b$ would be $1.369$, which gives us a competitive ratio of $129.744$.
Let us say that the label size is not known ahead of time, then we may choose to minimize $\frac{3b^4}{(b-1)^2(b+1)}$, such that our minimum is at $b = 2.563$, and our competitive ratio is : $29.8 \cdot (2.563)^{4\cdot |L_{min}| + 15}$.
\\

\section{Summary}
\seclabel{summary}
\paragraph*{}
In conclusion, we show that these exists an upper bound of $\frac{3b^4 D}{(b-1)^2(b+1)} \cdot b^{2\cdot O(|L_{min}|)}$ for rendezvous on a line with unknown initial distance, and when calculating the the competitive ratio this becomes $\frac{3b^4}{(b-1)^2(b+1)} \cdot b^{2\cdot O(|L_{min}|)}$ such that it is constant in respect to $D$, but exponential in respect to $|L_{min}|$. We know that this is not optimal since depending on the value of $D$ chosen other algorithms seen in \cite{test2,test3} will preform better. It would be interesting to see if there is an algorithm that does not depend on a label, however, it is unlikely since we use the labels to prove that our algorithms differ, by the fact that our labels are distinct.
\paragraph*{}
Furthermore, with closer analysis the convergent bit of $O(|L_{min}|)$ may be improved, most notably in the case when $|L_{min}| = |L_{max}|$. In addition, it would be interesting to see if a smaller label transformation is possible while maintaining that there exists a convergent bit.
\section*{Acknowledgement}


\bibliography{template}{}
\bibliographystyle{plain}


\end{document}
